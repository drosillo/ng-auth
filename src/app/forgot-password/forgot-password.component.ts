import { Component, OnInit } from '@angular/core';

import { User } from '../models/user';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  user: User = {};
  resetRequested: boolean = false;

  constructor(
      private auth: AuthService
  ) { }

  ngOnInit(): void {
  }

  requestPasswordResetLink(): void {
    this.auth.requestPasswordResetLink(this.user).subscribe(() => {
      this.resetRequested = true;
    });
  }

}
