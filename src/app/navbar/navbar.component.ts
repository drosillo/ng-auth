import { Component, OnInit } from '@angular/core';

import { environment } from '../../environments/environment';

import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  title = environment.title;

  // We use $ as a convention to define currentUser as an observable
  currentUser$: User = {};
  authenticated$: boolean = false;

  constructor(
      private auth: AuthService,
      private userService: UserService
  ) {
    // We subscribe to currentUser and watch for changes
    this.userService.userObservable.subscribe(user => {
      this.currentUser$ = user;
    });
    this.auth.authenticated.subscribe(value => {
      this.authenticated$ = value;
    });
  }

  ngOnInit(): void {
  }

  logout(e: any): void {
    e.preventDefault();
    e.stopPropagation();

    this.auth.logout().subscribe(() => {
      this.auth.clearSession();
    });
  }

}
