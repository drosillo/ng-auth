import { Injectable } from '@angular/core';
import {
    HttpInterceptor, HttpHandler, HttpRequest, HttpXsrfTokenExtractor, HttpResponse
} from '@angular/common/http';
import { tap } from 'rxjs/operators';

import { StorageService } from './services/storage.service';
import { environment } from '../environments/environment';

@Injectable()

export class Interceptor implements HttpInterceptor {

    headerName: string = 'X-XSRF-TOKEN';

    constructor(
        private tokenService: HttpXsrfTokenExtractor,
        private storage: StorageService
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'X-XSRF-TOKEN': ''
        };

        // We get the token from the response headers
        const token = this.tokenService.getToken();

        // We add the XSRF token to the custom headers. We do this because Angular will not add it
        // for GET or HEAD requests and absolute paths, which we're using.
        if (token !== null && !req.headers.has(this.headerName)) {
            headers['X-XSRF-TOKEN'] = token;
        }

        const authReq = req.clone({
            setHeaders: headers,
            withCredentials: true
        });

        return next.handle(authReq).pipe(
            tap((response: any) => {
                if (response instanceof HttpResponse) {
                    if (response.url?.includes(environment.authURL)) {
                        this.storage.refreshExpiration();
                    }
                }
            })
        );
    }
}