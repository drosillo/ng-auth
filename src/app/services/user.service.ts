import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';

import { User } from '../models/user';
import { environment } from '../../environments/environment';

import {Observable, Subject, throwError} from 'rxjs';
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiURL: string = environment.apiURL;

  userObservable: Subject<User> = new Subject<User>();
  currentUser: User = {};

  constructor(
      private http: HttpClient
  ) {
    this.userObservable.subscribe((user) => {
      this.currentUser = user;
    });
  }
  
  getUser(): Observable<User> {
    return this.http.get<User>(this.apiURL + '/user').pipe(
        catchError(this.handleError)
    );
  }

  setCurrentUser(user: User, refresh?: boolean): void {
    this.userObservable.next(user);
    if (refresh) {
      this.http.get<User>(this.apiURL + '/user').subscribe(data => {
        // We push the user data fetched from the database to the currentUser observable
        this.userObservable.next(data);
      });
    }
  }

  update(user: User): Observable<User> {
    return this.http.put<User>(this.apiURL + '/user', {
      name: user.name,
      email: user.email
    });
  }

  updatePassword(current_password: string, user: User): Observable<void> {
    return this.http.put<void>(this.apiURL + '/user/password', {
      current_password,
      password: user.password,
      password_confirmation: user.password_confirmation
    })
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
