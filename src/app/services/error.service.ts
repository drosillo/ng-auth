import { Injectable } from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  processServerErrors(form: FormGroup, response: any): void {
    if (response instanceof HttpErrorResponse) {
      if (response.status === 422) {
        const errors = response.error.errors;
        // Data sent to the server was invalid
        Object.keys(errors).forEach(field => {
          const control = this.getFieldControl(form, field);
          const message = errors[field];
          if (control && this.hasField(form, field)) {
            this.setFieldError(form, field, message);
          }
        });
      }
    }
  }

  hasField(form: FormGroup, field: string): boolean {
    const control = this.getFieldControl(form, field);
    return control !== null;
  }

  setFieldError(form: FormGroup, field: string, message: string): void {
    const control = this.getFieldControl(form, field);
    control.setErrors({
      serverError: message
    });
  }

  fieldHasError(form: FormGroup, field: string): boolean {
    const control = this.getFieldControl(form, field);

    if (control && control.touched && control.errors) {
      return this.getErrors(control).length > 0;
    } else {
      return false;
    }
  }

  getFieldError(form: FormGroup, field: string): string[] {
    const control = this.getFieldControl(form, field);

    return this.getErrors(control);
  }

  getErrors(control: AbstractControl): string[] {
    const errors = <string[]>[];
    for (const key in control.errors) {
      errors.push(control.errors[key]);
    }
    return errors;
  }

  private getFieldControl(form: FormGroup, field: string): AbstractControl {
    return <AbstractControl>form.get(field);
  }

}
