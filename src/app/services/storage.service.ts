import { Injectable } from '@angular/core';

// @ts-ignore
import { DateTime } from 'luxon';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  saveSession() {
    localStorage.setItem('authenticated', 'true');
    this.refreshExpiration(true);
  }

  getExpiration(): string | null {
    return localStorage.getItem('expiresAt');
  }

  hasExpiration(): boolean {
    return localStorage.getItem('expiresAt') !== null;
  }

  refreshExpiration(force?: boolean): void {
    if (this.hasExpiration() || force) {
      let expires = DateTime.now().plus({ minutes: 120 }).toISO();
      localStorage.setItem('expiresAt', expires);
    }
  }

  clear() {
    localStorage.clear();
  }
}
