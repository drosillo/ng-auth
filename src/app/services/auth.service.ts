import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';

import { Router } from '@angular/router';

import { environment } from '../../environments/environment';
import {Observable, Subject, throwError} from 'rxjs';

import { User } from '../models/user';
import { UserService } from './user.service';
import { StorageService } from './storage.service';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  backendURL: string = environment.authURL;

  // OBSERVABLES
  // Define authenticated as an observable other services and components can subscribe to
  // and watch for changes
  authenticated: Subject<boolean> = new Subject<boolean>();
  isAuthenticated: boolean = false;

  constructor(
      private http: HttpClient,
      private userService: UserService,
      private router: Router,
      private storage: StorageService
  ) {
    this.authenticated.subscribe((value) => {
      this.isAuthenticated = value;
    });
  }

  sanctumCSRF(): Observable<void> {
    return this.http.get<void>(this.backendURL + '/sanctum/csrf-cookie');
  }

  login(user: User): Observable<User> {
    return this.http.post<User>(this.backendURL + '/login', {
      email: user.email,
      password: user.password
    }).pipe(
        catchError(this.handleError)
    );
  }

  logout(): Observable<void> {
    return this.http.post<void>(this.backendURL + '/logout', {});
  }

  register(user: User): Observable<User> {
    return this.http.post<User>(this.backendURL + '/register', {
      name: user.name,
      email: user.email,
      password: user.password,
      password_confirmation: user.password_confirmation,
    }).pipe(
        catchError(this.handleError)
    );
  }

  setAuthenticated(value: boolean): void {
    this.authenticated.next(value);
  }

  clearSession(): void {
    this.userService.userObservable.next({});
    this.setAuthenticated(false);
    this.storage.clear();
    this.router.navigate(['login']);
  }

  requestPasswordResetLink(user: User): Observable<void> {
    return this.http.post<void>(this.backendURL  + '/forgot-password', {
      email: user.email
    });
  }

  resetPassword(token: string | null, user: User): Observable<void> {
    return this.http.post<void>(this.backendURL + '/reset-password', {
      token: token,
      email: user.email,
      password: user.password,
      password_confirmation: user.password_confirmation
    })
  }

  postLogin(user: User, refreshUser?: boolean): void {
    this.setAuthenticated(true);
    this.userService.setCurrentUser(user, refreshUser);
    this.storage.saveSession();
    this.router.navigate(['profile']);
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

}
