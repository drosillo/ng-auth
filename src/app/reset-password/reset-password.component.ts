import { Component, OnInit } from '@angular/core';

// This will allow us to access query params
import { ActivatedRoute } from '@angular/router';

import { User } from '../models/user';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  token: string | null;
  user: User = {};

  constructor(
      private route: ActivatedRoute,
      private auth: AuthService
  ) {
    // Get the token query param
    this.token = this.route.snapshot.queryParamMap.get('token');
    // Get the email query param
    // @ts-ignore
    this.user.email = this.route.snapshot.queryParamMap.get('email');
  }

  ngOnInit(): void {
  }

  resetPassword(): void {
    this.auth.resetPassword(this.token, this.user).subscribe();
  }

}
