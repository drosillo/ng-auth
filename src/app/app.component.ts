import { Component, OnInit } from '@angular/core';

import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { ErrorService } from './services/error.service';
import { StorageService } from './services/storage.service';
// @ts-ignore
import { DateTime } from 'luxon';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
      private auth: AuthService,
      private userService: UserService,
      private errorService: ErrorService,
      private storage: StorageService
  ) {
    // Call the CSRF API endpoint when the main component is initialized so we have the XSRF cookie set onwards
    this.auth.sanctumCSRF().subscribe();
  }

  ngOnInit(): void {
    this.checkValidSession();
  }

  checkValidSession() {
    const now = DateTime.now();
    let expiresAt = this.storage.getExpiration();

    if (expiresAt !== null) {
      expiresAt = DateTime.fromISO(expiresAt);
    } else {
      expiresAt = now;
    }

    // @ts-ignore
    if (now < expiresAt) {
      this.auth.postLogin({}, true);
    } else {
      this.auth.clearSession();
    }
  }
}
