import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// @ts-ignore
import _ from 'lodash';

import authRoutes from './routes/auth';
import profileRoutes from './routes/profile';

const routes: Routes = _.concat(
    [
      { path: '', redirectTo: '/login', pathMatch: 'full' },
    ],
    authRoutes,
    profileRoutes
);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
