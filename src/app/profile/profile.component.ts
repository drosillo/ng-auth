import { Component, OnInit } from '@angular/core';

import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  currentUser$: User = {};

  constructor(
      private userService: UserService
  ) {
    this.userService.userObservable.subscribe(user => {
      // We make a deep clone of the user so other components that are observing it don't catch our changes in real time
      this.currentUser$ = {...user};
    });
  }

  ngOnInit(): void {
    // We leave this here so currentUser$ can have a value set when navigating into the profile.
    // Why? I don't know, it just works this way...
    this.currentUser$ = {...this.userService.currentUser};
  }

  update(): void {
    this.userService.update(this.currentUser$).subscribe((user: User) => {
      this.userService.setCurrentUser(user);
    });
  }

}
