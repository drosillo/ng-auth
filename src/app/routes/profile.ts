import { ProfileComponent } from '../profile/profile.component';

import { AuthGuard } from '../guards/auth.guard';

const profileRoutes: any[] = [
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard]
    }
];

export default profileRoutes;