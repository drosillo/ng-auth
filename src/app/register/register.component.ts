import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { User } from '../models/user';

import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { ErrorService } from '../services/error.service';
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user: User = {};

  registerForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
    password_confirmation: ['', Validators.required]
  });

  constructor(
      private auth: AuthService,
      private userService: UserService,
      private fb: FormBuilder,
      public errorService: ErrorService
  ) { }

  ngOnInit(): void {
  }

  register(): void {
    const form = this.registerForm.value;
    this.auth.register(form as User).subscribe((user: User) => {
      this.auth.postLogin(user);
    }, (response) => {
      this.registrationError(response);
    });
  }

  registrationError(response: any) {
    this.errorService.processServerErrors(this.registerForm, response);
  }

}
