import { Component, OnInit } from '@angular/core';

import { User } from '../models/user';

import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMessage: string = '';

  user: User = {
    email: '',
    password: ''
  };

  constructor(
      private auth: AuthService,
      private userService: UserService
  ) { }

  ngOnInit(): void {
  }

  login() {
    this.auth.login(this.user).subscribe((user: User) => {
      this.auth.postLogin(user);
    }, (response) => {
      this.errorMessage = response.error.message;
    });
  }

}
