import { Component, OnInit } from '@angular/core';

import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {

  user$: User = {};
  currentPassword: string = '';

  constructor(
      private userService: UserService
  ) { }

  ngOnInit(): void {
  }

  updatePassword(): void {
    this.userService.updatePassword(this.currentPassword, this.user$).subscribe(() => {
      this.clearFields();
    });
  }

  clearFields(): void {
    this.user$ = {};
    this.currentPassword = '';
  }

}
