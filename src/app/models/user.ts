export interface User {
    id?: Number;
    name?: string;
    email?: string;
    email_verified_at?: Date;
    password?: string;
    password_confirmation?: string;
    created_at?: Date;
    updated_at?: Date;
}
